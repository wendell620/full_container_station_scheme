@echo off

:设置编码
CHCP 65001

:参数判定
if "%1"=="" (
echo [tag not input]

pause>nul

exit
)

:------------------------------ 配置 ------------------------------

:项目名称
set name=ykb_gateway

:项目端口
set projectPort=12000

:项目类型 php/java
set type=java

:环境 dev/test/release
set context=dev

:地址
set url=https://15000432144:qweasdzxcqqq@git.coding.net/yangy_noahark/ykb_gateway.git

:------------------------------ 配置 ------------------------------

:切换盘符
%~d0

:获取当前脚本路径
set dir=%~dp0

:引入通用服务配置
cd %dir%
call [000]default_server.bat

:进入对应环境目录[内网|集成|正式]
cd %dir%..\..\project\%context%

:判定项目文件夹是否存在
if exist .\%name% (
:删除已经存在的目录
rd /S/Q .\%name%
)

:拉取代码
call git clone -b %1 %url% %name%

:执行ant脚本 pjname 名称 pjcontext 环境 pjip 服务器IP pjport 端口 pjuser 用户 pjpassword 密码 pjrunport 容器端口
cd %dir%bin\ykb_standard
call ..\..\..\..\apache-ant-1.10.1\bin\ant -file .\ykb_standard_"%type%".xml -Dpjname="%name%" -Dpjcontext="%context%" -Dpjip="%imagesBuildServerIp%" -Dpjport="%imagesBuildServerPort%" -Dpjuser="%imagesBuildServerUser%" -Dpjpassword="%imagesBuildServerPwd%" -Dpjrunport="%projectPort%" -Dptag="%1" -Dprip="%imagesRegistryIp%" -Dprport="%imagesRegistryPort%"

:等待任意键退出
echo [press any key to exit]

pause>nul

exit