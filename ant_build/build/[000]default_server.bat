:------------------------------ 配置 ------------------------------

:镜像编译服务地址
set imagesBuildServerIp=

:镜像编译服务端口
set imagesBuildServerPort=

:镜像编译服务账号
set imagesBuildServerUser=

:镜像编译服务密码
set imagesBuildServerPwd=

:镜像库地址
set imagesRegistryIp=

:镜像库端口
set imagesRegistryPort=

:yml-k8s访问IP
set k8sPortIp=

:------------------------------ 配置 ------------------------------